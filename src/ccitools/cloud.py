#!/usr/bin/python

import logging
import inspect
import json
import prettytable
import re

from aitools.common import get_openstack_environment
from aitools.openstack_auth_client import OpenstackAuthClient
from ccitools.errors import CciCloudNotFoundError
from cinderclient.v2 import client as cinder_client
from keystoneauth1 import session as keystone_session
from keystoneclient import exceptions
from keystoneclient.v3 import client as keystone_client
from novaclient import client as nova_client
from glanceclient.v2 import client as glance_client

logger = logging.getLogger(__name__)

class CloudClient():
    """
    Cloud client encapsulating the main operations in the OpenStack cloud. This
    class is designed as a facade to facilitate the interaction with all the
    components of the CERN OpenStack Cloud.
    """

    def __init__(self, cornerstone=None):
        """
        Initializes the facade to the Cloud operations.

        :param auth_client: OpenstackAuthClient that does the authentication
        """
        auth_client = OpenstackAuthClient(get_openstack_environment())
        session = auth_client.session
        self.keystone = keystone_client.Client(session=session)
        self.cinder = cinder_client.Client(session=session)
        self.glance = glance_client.Client(session=session)
        self.nova = nova_client.Client(version='2', session=session)
        self.cornerstone = cornerstone


    def __verify_quota_update(self, project_id, cores, instances, ram, volumes, volumes_gigabytes, volume_type):
        logger.info("Verifying quotas after the update...")
        nova_quota = self.nova.quotas.get(project_id)
        cinder_quota = self.cinder.quotas.get(project_id)
        args, _, _, values = inspect.getargvalues(inspect.currentframe())
        for arg in args:
            if (arg in nova_quota._info and int(nova_quota._info[arg]) != int(values[arg]) or
            (arg in cinder_quota._info and int(cinder_quota._info[arg]) != int(values[arg]))):
                raise Exception("Project quota does NOT match requested one. Something must have failed." )


    def add_flavor(self, project, flavor):
        self.nova.flavor_access.add_tenant_access(flavor, project)


    def has_flavor_access(self, project_name, flavor_name):
        project = self.find_project(project_name)
        flavor_list = self.nova.flavors.list(is_public=False) + self.inova.flavors.list(is_public=True)
        flavor = [x for x in flavor_list if x.name == flavor_name]
        if len(flavor) >= 1:
            if getattr(flavor[0], 'os-flavor-access:is_public'):
                return True

            access_list = self.nova.flavor_access.list(flavor=flavor[0])
            l = [x for x in access_list if x.tenant_id == project.id]
            return len(l) == 1

        raise CciCloudNotFoundError("Could not evaluate '%s'. Flavor not found." % (flavor_name))


    def set_project_property(self, project_name, property_name, property_value):
        project = self.find_project(project_name)
        r = self.cornerstone.project_properties_add(project.id, property_name, property_value)
        status_code = str(r.status_code)
        if not status_code.startswith('2'):
            raise Exception("Cornerstone failed setting '%s' property, error: %s" %
                           (property_name, json.loads(r.text)['property_value'][0]))


    def add_group_member(self, project, member_name):
        project_members = self.get_project_members(project.name)
        if member_name.lower() in (name.lower() for name in project_members):
            logger.info("Egroup '%s' already a member of '%s'" % (member_name, project.name))
            return

        logger.info("Adding group '%s' as member of '%s'..." % (member_name, project.name))
        r = self.cornerstone.add_project_members(project.id, "Member", member_name)
        status_code = str(r.status_code)
        if not status_code.startswith('2'):
            raise Exception("Cornerstone failed adding egroup '%s', error: %s" %
                           (property_name, json.loads(r.text)['property_value'][0]))

        logger.info("'%s'has been added to '%s'" % (member_name, project.name))


    def find_project(self, reference):
        """
        Finds a project by name or ID

        :param reference: either project name or ID
        """
        logger.info("Looking for project '%s'..." %  (reference))
        try:
            return self.keystone.projects.find(name=reference)
        except exceptions.NotFound:
            return self.keystone.projects.get(reference)


    def get_hypervisor_by_name(self, hypervisor_name):
        """
        Returns the hypervisor object

        :param hypervisor_name
        """
        if re.search("(.*).cern.ch", hypervisor_name):
            hypervisor_name = re.search("(.*).cern.ch", hypervisor_name).group(1)
        hvs = self.nova.hypervisors.search(hypervisor_name, True)
        # return exact match (hypervisor_hostname may or may not contain cern.ch)
        return [hv for hv in hvs if hv.hypervisor_hostname.split(".cern.ch")[0].lower() == hypervisor_name.lower()][0]


    def get_servers_by_hypervisor(self, hypervisor_name):
        """
        List all the servers running on the specified Hypervisor

        :param hypervisor_name
        """
        servers = []
        hypervisor = self.get_hypervisor_by_name(hypervisor_name)
        if hasattr(hypervisor, 'servers'):
            for server in hypervisor.servers:
                servers.append(self.nova.servers.get(server['uuid']))
        return servers


    def get_servers_by_project(self, project_name):
        """
        List all the servers running on the specified tenant

        :param project_name
        """
        project = self.find_project(project_name)
        return self.nova.servers.list(search_opts={'all_tenants': True, 'project_id': project.id})


    def get_project_members(self, project_name):
        """
        Returns a list of all the members of a project

        :param project_name
        """
        members = []
        logger.info("Trying to get members of '%s'..." %  (project_name))
        project = self.find_project(project_name)
        project_role_assignments = self.keystone.role_assignments.list(project=project)
        for project_role_assignment in project_role_assignments:
            if hasattr(project_role_assignment, 'user') and project_role_assignment.user['id'] not in members:
                members.append(project_role_assignment.user['id'])
                logger.debug("Found user '%s' as member of project '%s'" % (project_role_assignment.user['id'], project.name))
            if hasattr(project_role_assignment, 'group') and project_role_assignment.group['id'] not in members:
                members.append(project_role_assignment.group['id'])
                logger.debug("Found egroup '%s' as member of project '%s'" % (project_role_assignment.group['id'], project.name))
        return members


    def get_nova_service(self, hypervisor_name):
        """
        Returns the service running on the specified hypervisor

        :param hypervisor_name
        """
        hypervisor = self.get_hypervisor_by_name(hypervisor_name)
        host = "%s@%s" % (re.search('(.*)@(.*)', hypervisor.id).group(1), hypervisor.hypervisor_hostname)
        return self.nova.services.list(host=host)[0]


    def get_printable_quota(self, project_name):
        """
        Returns the quota for a given project (Cinder and Nova)

        :param project_name
        """
        t = prettytable.PrettyTable(["Service", "Quota", "Value"])
        project = self.find_project(project_name)
        nova_quota = self.nova.quotas.get(project.id)
        logger.info(nova_quota.__dict__)
        t.add_row(["Nova", "Cores", "%s" % (nova_quota.cores)])
        t.add_row(["Nova", "Instances", "%s" % (nova_quota.instances)])
        t.add_row(["Nova", "RAM", "%s GB" % (int(nova_quota.ram) / 1024)])

        cinder_quota = self.cinder.quotas.get(project.id)
        logger.info(cinder_quota.__dict__)
        t.add_row(["Cinder", "Diskspace on volumes", "%s GB" % (cinder_quota.gigabytes)])
        t.add_row(["Cinder", "Volumes", "%s" % (cinder_quota.volumes)])

        t.border = True
        t.header = True
        t.align["Field"] = 'r'
        t.align["Value"] = 'l'

        return t


    def get_region_name(self, hypervisor_name):
        """
        Returns the region of a hypervisor

        :param hypervisor_name
        """
        hypervisor = self.get_hypervisor_by_name(hypervisor_name)
        return re.search('father!(.*)@', hypervisor.id).group(1)


    def update_quota(self, project_id, cores, instances, ram, volumes, volumes_gigabytes, volume_type):
        """
        Updates the quota in Cinder and Nova for a given project.

        :project_id: the project id that will be updated.
        :cores: number of cores to be set in Nova.
        :instances: number of instances to be set in Nova.
        :ram: amount of ram to be set in Nova.
        :volumes: number of volumes in Cinder.
        :volumes_gigabytes: amount of gigabytes available in Cinder.
        """
        logger.info("Updating quota on project '%s'" % (project_id))
        self.update_quota_nova(project_id, cores=cores, instances=instances, ram=ram)
        self.update_quota_cinder(project_id, volumes, volumes_gigabytes, volume_type)
        self.__verify_quota_update(project_id, cores, instances, ram, volumes, volumes_gigabytes, volume_type)
        logger.info("Cinder and Nova quota updated successfully")


    def update_quota_global_cinder(self, project_id):
        updates = {}
        cinder_quota = self.cinder.quotas.get(project_id)

        global_volumes = 0
        global_gigabytes = 0

        for key in cinder_quota._info:
            if cinder_quota._info[key] != -1:
                if 'volumes_' in key:
                    global_volumes += int(cinder_quota._info[key])
                if 'gigabytes_' in key:
                    global_gigabytes += int(cinder_quota._info[key])

        updates['volumes'] = global_volumes
        updates['gigabytes'] = global_gigabytes

        self.cinder.quotas.update(project_id, **updates)
        logger.info("Cinder global updated, volumes: %s gigabytes: %s" % (global_volumes, global_gigabytes))

    def update_quota_cinder(self, project_id, volumes, gigabytes, volume_type):
        """
        Updates Cinder quota for a specific volume type and the global one accordingly.
        Only updates if the specified value and the current are different

        :project_id: the project id that will be updated.
        :volumes: number of volumes.
        :gigabytes: amount of gigabytes available
        :volume_type: Volume Type
        """
        updates = {}
        current_quota = self.cinder.quotas.get(project_id)
        if int(current_quota._info['volumes_%s' % volume_type]) != int(volumes):
            updates['volumes_%s' % volume_type] = volumes
        if int(current_quota._info['gigabytes_%s' % volume_type]) != int(gigabytes):
            updates['gigabytes_%s' % volume_type] = gigabytes

        if updates:
            self.cinder.quotas.update(project_id, **updates)
            logger.info("Cinder quota updated: %s" % updates)
            self.update_quota_global_cinder(project_id)
            logger.info("Cinder quota updated")
        else:
            logger.info("No changes in Cinder quota. Quota not updated")


    def update_quota_nova(self, project_id, cores, instances, ram):
        """
        Updates Nova quota only if the specified value and the current are
        different
        """
        updates = {}
        args, _, _, values = inspect.getargvalues(inspect.currentframe())
        current_quota = self.nova.quotas.get(project_id)
        for arg in args:
            if arg in current_quota._info and int(current_quota._info[arg]) != int(values[arg]):
                updates[arg] = values[arg]

        if updates:
            self.nova.quotas.update(project_id, **updates)
            logger.info("Nova quota updated: %s" % updates)
        else:
            logger.info("No changes in Nova quota. Quota not updated")
