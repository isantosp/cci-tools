#!/usr/bin/env python

import logging
import requests
import xml.etree.ElementTree as ET


class FIMClient:


    def __init__(self,url, sso_cookie):
        self.base_url = url
        self.cookies = sso_cookie


    def __send_request(self, url, method='GET', **kwargs):
        r = requests.request(method=method, url=url, cookies=self.cookies, **kwargs)
        if r.status_code != requests.codes.ok:
            raise Exception("The request: '%s' failed with status code '%s'" % (url, r.status_code))
        return r


    def create_project(self, domain, name, description, owner, shared=True):
        """
        Creates a new OpenStack Project using FIM Web Service

        :param domain: Project domain
        :param name: Project name
        :param description: Project description
        :param owner: Project owner
        :param shared: boolean
        """
        url = '%s/CreateProject' % self.base_url
        payload={'domain': domain, 'name': name, 'description': description, 'owner': owner, 'shared': shared}
        xml_response = self.__send_request(url=url, method='GET', params=payload).text
        return ET.fromstring(xml_response).text


    def enable_project(self, name):
        """
        Enables an OpenStack Project using FIM Web Service

        :param name: Project name
        """
        url = '%s/SetProjectStatus' % self.base_url
        payload={'name': name, 'enabled': 'True'}
        xml_response = self.__send_request(url=url, method='GET', params=payload).text
        return ET.fromstring(xml_response).text


    def is_valid_owner(self, username):
        """
        Checks if user is a valid OpenStack Project owner using the FIM Web Service

        :param username: User name
        """
        url = '%s/ValidateOwner' % self.base_url
        payload={'owner': username}
        xml_response = self.__send_request(url=url, method='POST', data=payload).text
        return True if ET.fromstring(xml_response).text == 'true' else False
