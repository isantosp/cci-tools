import logging

from smtplib import SMTP
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart

class MailClient:

    def __init__(self, smtp_server='localhost'):
        self.smtp_server = smtp_server

    def __create_mail(self, mail_to, mail_subject, mail_body, mail_from, mail_cc, mail_bcc, mail_content_type): 
        msg = MIMEMultipart()
        msg['To'] = mail_to
        msg['From'] = mail_from
        msg['Cc'] = mail_cc
        msg['Bcc'] = mail_bcc
        msg['Subject'] = mail_subject
        msg.attach(MIMEText(mail_body, mail_content_type))
        return msg

    def send_mail(self, mail_to, mail_subject, mail_body, mail_from, mail_cc, mail_bcc, mail_content_type):
        logging.debug("Sending email to '%s'..." % (mail_to))
        msg = self.__create_mail(mail_to, mail_subject, mail_body, mail_from, mail_cc, mail_bcc, mail_content_type)
        smtpObj = SMTP(self.smtp_server)
        smtpObj.sendmail(msg['From'], [msg['To'], msg['Cc'], msg['Bcc']], msg.as_string())
        smtpObj.quit()
