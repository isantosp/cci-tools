#!/usr/bin/env python

import logging
import re

from ccitools.errors import CciSnowRecordNotFoundError
from ccitools.errors import CciSnowTicketError
from ccitools.errors import CciSnowTicketStateError
from ccitools.static.servicenow_codes import INCIDENT_STATUS_CODE, REQUEST_STATUS_CODE
from ccitools.static.servicenow_project_create import PROJECT_CREATION_RP, PROJECT_CREATION_RP_MAPPING
from ccitools.static.servicenow_quota_update import QUOTA_UPDATE_RP, QUOTA_UPDATE_RP_MAPPING
from suds.client import Client

logger = logging.getLogger(__name__)

class ProjectRequestRP:
    fields = PROJECT_CREATION_RP
    def __init__(self, question_answer_list):
        self.record = question_answer_list

    def __getattr__(self, name):
        if name in self.fields:
            value = next((entry for entry in self.record if entry.question == self.fields[name])).value
            # if department==Other return value from field 'other'
            if name == 'department' and value == 'Other':
                value = next((entry for entry in self.record if entry.question == self.fields['other'])).value
            # if accounting_group==Other return value from field 'other'
            elif name == 'accounting_group' and value == 'Other':
                value = next((entry for entry in self.record if entry.question == self.fields['other_ag'])).value

            if value:
                return value.strip()
            else:
                return ""
        else:
            raise AttributeError("Project Request RP does not have '%s' attibute" % name)


class QuotaUpdateRequestRP:
    fields = QUOTA_UPDATE_RP
    def __init__(self, question_answer_list):
        self.record = question_answer_list

    def __getattr__(self, name):
        if name in self.fields:
            if name == 'volume_quota':
                volume_quota = []
                for v in self.fields['volume_quota']:
                    d = {}
                    try:
                        if (next((entry for entry in self.record if entry.question == v['volumes'])).value != None or
                            next((entry for entry in self.record if entry.question == v['gigabytes'])).value != None):
                            for k in v:
                                value = next((entry for entry in self.record if entry.question == v[k])).value
                                if value:
                                    d[k] = value.strip()
                                else:
                                    d[k] = None

                            volume_quota.append(d)
                    except Exception:
                       logger.warning("Value of '%s' empty" % v['visible'])
                return volume_quota

            else:
                value = next((entry for entry in self.record if entry.question == self.fields[name])).value
                if value:
                    return value.strip()
                else:
                    return None
        else:
            raise AttributeError("Quota update Request RP does not have '%s' attibute" % name)


class ServiceNowClient:
   
    def __init__(self, username, password, assignment_group=None, instance='cern'):
        self.username = username
        self.instance = instance
        self.assignment_group = assignment_group
        self.requests = self.__get_service("u_request_fulfillment", username, password)
        self.incidents = self.__get_service("incident", username, password)
        self.record_producers = self.__get_service("question_answer_list", username, password)
        self.users = self.__get_service("sys_user_list", username, password)
        self.assignment_groups = self.__get_service("sys_user_group", username, password)
        self.user_groups = self.__get_service("sys_user_grmember", username, password)
        self.functional_elements = self.__get_service("u_cmdb_ci_functional_services", username, password)
        self.rel_ci = self.__get_service("cmdb_rel_ci", username, password)


    def __filter_table(self, query, table):
        msg = \
          """<?xml version="1.0" encoding="UTF-8"?>
          <SOAP-ENV:Envelope xmlns:ns0="http://www.service-now.com/incident"
              xmlns:ns1="http://schemas.xmlsoap.org/soap/envelope/"
              xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
              xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/">
          <SOAP-ENV:Header/>
              <ns1:Body>
                  <ns0:getRecords>
                      <ns0:__encoded_query>%s</ns0:__encoded_query>
                  </ns0:getRecords>
              </ns1:Body>
          </SOAP-ENV:Envelope>
            """ % (query)
        return table.getRecords(__inject = {'msg': msg})


    def __get_groups_by_user(self, user):
        """ Return list assignment groups IDs the user belongs to"""
        return [user_group.group for user_group in self.user_groups.getRecords(user=user)]


    def __get_incident_status_code(self, state):
        """ Gets incident status code value """
        if state in INCIDENT_STATUS_CODE.keys():
            return INCIDENT_STATUS_CODE[state]
        raise CciSnowTicketStateError("'%s' is not a valid incident state" % state)


    def __get_request_status_code(self, state):
        """ Gets request status code value """
        if state in REQUEST_STATUS_CODE.keys():
            return REQUEST_STATUS_CODE[state]
        raise CciSnowTicketStateError("'%s' is not a valid request state" % state)


    def __get_service(self, table, username, password):
        url = 'https://%s.service-now.com/%s.do?WSDL' % (self.instance, table)
        logger.debug("Initializing table '%s' on '%s'" % (table, url))
        return Client(url, username=username, password=password).service


    def __is_auth_to_edit(self, ticket, username):
        """ Checks if ticket assignment_group is one of user's groups"""
        groups_ids = self.__get_groups_by_user(username)
        if ticket.assignment_group in groups_ids:
            return True
        return False


    def __is_incident(self, number):
        return re.match("INC\d{7}", number)


    def __is_request(self, number):
        return re.match("RQF\d{7}", number)


    def __update_ticket(self, number, **kwargs):
        """
        Updates the information of a given ticket. It automatically
        extracts the sys_id of the given ticket to be used as the
        key of the update.

        It also detects if it's an incident or request.

        :param kwargs: Set of properties to update in the ticket.
        :raise CciSnowTicketError:
        """
        logger.info("Updating ticket '%s' with data: '%s'" % (number, kwargs))
        ticket = self.get_ticket(number)
        self.__validate_ticket(ticket)

        original_assignee = ticket.assigned_to
        self.change_assignee(number, self.username)

        try:
            if self.__is_request(number):
                self.requests.update(sys_id=ticket.sys_id, **kwargs)
            elif self.__is_incident(number):
                self.incidents.update(sys_id=ticket.sys_id, **kwargs)
            else:
                raise CciSnowTicketError("%s is not a request nor an incident" % (number))
        finally:

            if original_assignee:
                self.change_assignee(number, original_assignee)
            else:
                self.change_ticket_state(number, "assigned")

    def __validate_ticket(self, ticket):
        if not ticket.active:
            raise CciSnowTicketError("%s no longer active. Please verify the ticket number." % (ticket.number))
        if not self.__is_auth_to_edit(ticket, self.username):
            ticket_assignment_group = self.assignment_groups.getRecords(sys_id=ticket.assignment_group)[0].name
            raise CciSnowTicketError("%s is assigned to a group ('%s') the user '%s' does not belong to. Please verify the ticket number." % (ticket.number, ticket_assignment_group, self.username))


    def add_comment(self, number, comment):
        """
        Writes a comment in the given ticket

        :param number: ticket number
        :param comment: comment to be added to the ticket
        """
        self.__update_ticket(number, comments=comment)


    def add_email_watch_list(self, number, email_address):
        """
        Adds email to the ticket Watch List

        :param number: Ticket number.
        :param email_address:
        """
        watch_list = self.get_ticket(number).watch_list
        if not watch_list:
            watch_list = email_address
            self.__update_ticket(number, watch_list=watch_list)
        elif email_address not in watch_list.split(','):
            watch_list = "%s,%s" % (email_address, watch_list)
            self.__update_ticket(number, watch_list=watch_list)
        else:
            logger.warning("Email '%s' already member of '%s' watch list..." % (email_address, number))


    def add_user_watch_list(self, number, username):
        """
        Adds username to the ticket Watch List

        :param number: Ticket number.
        :param username:
        :raise CciSnowRecordNotFoundError:
        """
        logger.info("Adding user '%s' to '%s' watch list..." % (username, number))
        user = self.users.getRecords(user_name=username)
        if user:
            watch_list = self.get_ticket(number).watch_list
            if not watch_list:
                watch_list = user[0].sys_id
                self.__update_ticket(number, watch_list=watch_list)
            elif user[0].sys_id not in watch_list.split(','):
                watch_list = "%s,%s" % (user[0].sys_id, watch_list)
                self.__update_ticket(number, watch_list=watch_list)
            else:
                logger.warning("User '%s' already member of '%s' watch list..." % (username, number))
        else:
            error_msg = "Impossible to add '%s' to '%s' watchlist. User not found" % (username, number)
            logger.error(error_msg)
            raise CciSnowRecordNotFoundError(error_msg)


    def create_project_creation(self, number, data):
        """
        Convers the given Service-Now request ticket into
        a 'Request for Compute Resources' Record Producer.

        :param number: Request ticket number
        :param data: Python dict with values to create the RP.
        """
        ticket = self.get_ticket(number)
        ticket_id = ticket.sys_id
        table = ticket.sys_class_name

        for element_id, element_attibutes in PROJECT_CREATION_RP_MAPPING.iteritems():
            value = None
            if 'label' in element_attibutes.keys():
                label = element_attibutes['label']
                if label in data:
                    value = data[label]
                else:
                    logger.warning("Key %s not found in given data" % label)
            try:
                logger.info("Inserting element id '%s' with value '%s'..." % (element_id, value))
                self.record_producers.insert(order=element_attibutes['order'], question=element_id,
                                         table_name=table, table_sys_id=ticket_id, value=value)
            except Exception, err:
                logger.error("Error inserting '%s'. Error: %s" % (element_id, err))

        self.__update_ticket(number, u_caller_id=data['username'], description=data['comments'],
                             u_record_producer="dfb4225078a27100de14a0934907b597",
                             comments="%s \n\nCloud Service Automation (on behalf of the user)" % (data['comments']))


    def create_quota_update(self, number, volume_list, data):
        ticket = self.get_ticket(number)
        ticket_id = ticket.sys_id
        table = ticket.sys_class_name

        for element_id, element_attibutes in QUOTA_UPDATE_RP_MAPPING.iteritems():
            value = None
            if 'label' in element_attibutes.keys():
                label = element_attibutes['label'].split('_')
                if label[0] in volume_list:
                    volume_type = label[0]
                    if len(label) == 2: #volumes && diskspace
                        if volume_type in data['volumes']:
                            field = label[1]
                            value = data['volumes'][volume_type][field]
                    else: # dropdownmenu element
                        value = volume_type

                elif label[0] == 'visible':
                    if label[1] in data['volumes']:
                        value = 'Yes'
                    else:
                        value = 'No'
                elif label[0] in data:
                    value = data[label[0]]

                else:
                    logger.warning("Key %s not found in given data" % label)

            try:
                self.record_producers.insert(order=element_attibutes['order'], question=element_id,
                                         table_name=table, table_sys_id=ticket_id, value=value)
            except Exception, err:
                logger.error("Error inserting '%s'. Error: %s" % (element_id, err))

        self.__update_ticket(number, u_caller_id=data['username'], description=data['comments'],
                             u_record_producer="dfb4225078a27100de14a0934907b597",
                             comments="%s \n\nCloud Service Automation (on behalf of the user)" % (data['comments']))


    def create_incident(self, short_description, functional_element, **kwargs):
        if "assignment_group" in kwargs:
            kwargs['assignment_group'] = self.get_assignment_group_by_name(kwargs['assignment_group']).sys_id
        return self.incidents.insert(short_description=short_description,
                                    u_functional_element=functional_element,
                                    **kwargs)


    def create_request(self, short_description, functional_element, **kwargs):
        if "assignment_group" in kwargs:
            kwargs['assignment_group'] = self.get_assignment_group_by_name(kwargs['assignment_group']).sys_id
        return self.requests.insert(short_description=short_description,
                                    u_functional_element=functional_element,
                                    **kwargs)


    def add_work_note(self, number, work_note):
        """
        Writes a work note in the given ticket

        :param number: ticket number
        :param work_note: work note to be added to the ticket
        """
        self.__update_ticket(number, work_notes=work_note)


    def change_assignee(self, number, username):
        """
        Assign the ticket to the given username

        :param number: Ticket number
        :param username:
        """
        logger.info("Assigning ticket '%s' to '%s'" % (number, username))
        ticket = self.get_ticket(number)

        if self.__is_request(number):
            if ticket.u_current_task_state != self.__get_request_status_code('resolved'):
                self.requests.update(sys_id=ticket.sys_id, assigned_to=username)
        elif self.__is_incident(number):
            if ticket.incident_state != self.__get_incident_status_code('resolved'):
                self.incidents.update(sys_id=ticket.sys_id, assigned_to=username)
        else:
            raise CciSnowTicketError("%s is not a request nor an incident" % (number))


    def change_functional_element(self, number, functional_element_name, assignment_group_name):
        """
        Updates functional element on a ticket.
        To avoid leaving inconsistencies on the ticket also updates the service element and the
        assignment_group.
        """
        fe = self.get_functional_services_by_name(functional_element_name)
        se = self.get_best_service_element_for_fe(fe)
        group_id = self.get_assignment_group_by_name(assignment_group_name).sys_id
        assignment_group = None
        for v in fe:
            if v[0].find("u_support_line") != -1:
                if v[1] == group_id:
                    assignment_group = assignment_group_name
                    logger.debug("'%s' belongs to '%s' -> '%s'" %
                                 (assignment_group_name, functional_element_name, v[0]))
                    break
        if not assignment_group:
            raise CciSnowTicketError("'%s' does not belong to support line in '%s'" %
                                    (assignment_group_name, functional_element_name))

        logger.info("Changing ticket '%s' SE to '%s', FE to '%s' and assignment_group to '%s'"
                     % (number, se.sys_id, functional_element_name, assignment_group))
        self.__update_ticket(number, u_business_service=se.parent, u_functional_element=fe.sys_id,
                             u_ci_relations=se.sys_id, assignment_group=assignment_group)


    def change_ticket_state(self, number, state):
        """
        Set ticket state to assinged. Only of the ticket is not resolved.

        :param number: Ticket number
        :param number: Ticket state
        """
        logging.info("Change ticket '%s' state to %s" % (number, state))
        ticket = self.get_ticket(number)
        if self.__is_request(number):
            if int(ticket.u_current_task_state) != self.__get_request_status_code('resolved'):
                self.requests.update(sys_id=ticket.sys_id,
                                     u_current_task_state=self.__get_incident_status_code(state))
        elif self.__is_incident(number):
            if int(ticket.incident_state) != self.__get_incident_status_code('resolved'):
                self.incidents.update(sys_id=ticket.sys_id,
                                      incident_state=self.__get_incident_status_code(state))
        else:
            raise CciSnowTicketError("%s is not a request nor an incident" % (number))


    def get_assignment_group_by_name(self, assignment_group_name):
        records_list = self.assignment_groups.getRecords(name=assignment_group_name)
        if records_list:
            return records_list[0]
        else:
            raise CciSnowRecordNotFoundError("User group '%s' not found" % assignment_group_name)


    def get_best_service_element_for_fe(self, fe):
        """
        Selects the best Service Element for a given Functional Emlement

        :param fe: functional element record
        """
        rel_ci_records_list = self.rel_ci.getRecords(child=fe.sys_id)
        best_se = None
        for rel_ci_record in rel_ci_records_list:
            if rel_ci_record.u_name[0:1] == "A":
                best_se = rel_ci_record
            if rel_ci_record.u_name[0:2] == "A+":
                return rel_ci_record
        return best_se


    def get_functional_services_by_name(self, functional_element_name):
        records_list = self.functional_elements.getRecords(name=functional_element_name)
        if records_list:
            return records_list[0]
        raise CciSnowRecordNotFoundError("FE '%s' not found" % functional_element_name)


    def get_project_request_rp(self, number):
        """
        Returns an object containing the properties of the record producer

        :params number: Ticket number
        :returns: object containing the record producer properties
        """
        records =  self.get_record_producer(number)
        return ProjectRequestRP(records)


    def get_quota_update_request_rp(self, number):
        """
        Returns an object containing the properties of the record producer

        :params number: Ticket number
        :returns: object containing the record producer properties
        """
        records =  self.get_record_producer(number)
        return QuotaUpdateRequestRP(records)

    def get_record_producer(self, number):
        """
        Returns a list of answers to the questions in the record producer
        associated with the given ticket number.

        :param number: ServiceNow ticket number
        :return: list of answers
        """
        request = self.get_ticket(number)
        return self.record_producers.getRecords(table_sys_id=request.sys_id)


    def get_record_producer_by_field(self, number, field):
        records = self.get_record_producer(number)
        matches = next((entry for entry in records if entry.question == field))
        return matches.value


    def get_tickets_by_host(self, host):
        """
        Returns a list of ACTIVE tickets where the given host is specified as "Configuration item".
        """
        query = "active=true^u_configuration_items_listLIKE%s" % host
        return self.__filter_table(query, self.incidents)


    def get_ticket(self, number):
        """
        Returns a ticket entry based on the number. It checks if the given
        number matches an incident or request.

        :param number: ticket number
        """
        if self.__is_request(number):
            return self.requests.getRecords(number=number)[0]
        elif self.__is_incident(number):
            return self.incidents.getRecords(number=number)[0]
        else:
            raise CciSnowTicketError("%s is not a request nor an incident" % (number))


    def resolve_ticket(self, number, comment, resolver):
        """
        Resolves the ticket passed as argument

        :params number: Ticket number
        :params comment: Closing message
        :params resolver: User who resolves the ticket
        """
        ticket = self.get_ticket(number)

        if not self.__is_auth_to_edit(ticket, resolver):
           raise CciSnowTicketError("User '%s' does not belong to ticket's assignment group and therefore cannot resolve it" % resolver)

        if self.__is_request(number):
            resolved_status_code = self.__get_request_status_code('resolved')
            if int(ticket.u_current_task_state) != resolved_status_code:
                self.__update_ticket(number, u_close_code="Fulfilled", u_workflow_stage="Resolved",
                                     assigned_to=resolver, comments=comment, u_current_task_state=resolved_status_code)
                logger.info("'%s' status code set as '%s'" % (number, resolved_status_code))
            else:
                logger.info("'%s' status already set as resolved" % number)

        elif self.__is_incident(number):
            resolved_status_code = self.__get_incident_status_code('resolved')
            if int(ticket.incident_state) != resolved_status_code:
                self.__update_ticket(number, u_close_code="Restored", incident_state=resolved_status_code,
                                     assigned_to=resolver, comments=comment, state=resolved_status_code)
                logger.info("'%s' status code set as '%s'" % (number, resolved_status_code))
            else:
                logger.info("'%s' status already set as resolved" % number)

        else:
            raise CciSnowTicketError("%s is not a request nor an incident" % (number))


    def waiting_for_user(self, number):
        """
        Updates ticket status to 'waiting for user'

        :param number: Ticket number
        """
	if self.__is_request(number):
	    waiting_for_user_code = self.__get_request_status_code('waiting_for_user')
	elif self.__is_incident(number):
	    waiting_for_user_code = self.__get_incident_status_code('waiting_for_user')

	self.__update_ticket(number, u_current_task_state=waiting_for_user_code)
