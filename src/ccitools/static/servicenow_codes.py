INCIDENT_STATUS_CODE = {
  'new': 1,
  'assigned': 2,
  'in_progress': 3,
  'waiting_for_user': 4,
  'waiting_for_3rd_party': 5,
  'resolved': 6,
  'closed': 7,
  'waiting_for_change': 8,
  'waiting_for_parent': 9,
  'workaround_waiting_for_3rd_party': 10,
  'workaround_waiting_for_change': 11,
  'waiting_for_detail_task': 13,
}

REQUEST_STATUS_CODE = {
  'new': 1,
  'assigned': 2,
  'accepted': 3,
  'in_progress': 4,
  'waiting_for_user': 5,
  'waiting_for_3rd_party': 6,
  'waiting_for_project_mgmt': 7,
  'waiting_for_change_mgmt': 8,
  'resolved': 9,
  'closed': 10,
  'archived': 11,
  'waiting_for_delivery': 12,
  'waiting_for_detail_task': 13,
  'waiting_for_work_task': 15,
  'dispatch': "Dispatch",
  'waiting_for_approvals': "waiting_approvals"
}
