QUOTA_UPDATE_RP_MAPPING = {
     '75ffb13c115530404bafa32c34fb163e': {
        'order': '0',
    },
    '48010e3c115530404bafa32c34fb16fe': {
        'order': '100',
    },
    'd3af21794f0c9e00e3a2119f0310c752': {
        'order': '200',
    },
    '9f56bd484f585e00398ebc511310c75d': {
        'order': '400',
    },
    '9698a25078a27100de14a0934907b5f9': {
        'order': '500',
    },
    '5eb8265078a27100de14a0934907b5bb': {
        'order': '600',
    },
    '9bc8ea5078a27100de14a0934907b56b': {
        'order': '700',
    },
    'd919ae5078a27100de14a0934907b5dc': {
        'order': '1000',
    },
    '3469629078a27100de14a0934907b5d1': {
        'order': '1200',
    },
    'c51935f94f0c9e00e3a2119f0310c76f': {
        'order': '1300',
    },
    '86c5710e4fd0de00398ebc511310c7a9': {
        'order': '1400',
    },
    'a55ae29078a27100de14a0934907b5ca': {
        'order': '1500',
    },
    'c88a2a9078a27100de14a0934907b5a5': {
        'order': '1600',
    },
    '5e7602282b8d42004baf9d8269da1588': {
        'order': '1700',
    },
    'f0b682282b8d42004baf9d8269da1520': {
        'order': '2000',
    },
    'e6ca2a9078a27100de14a0934907b53a': {
        'order': '2200',
    },
    '71ca79f94f0c9e00e3a2119f0310c794': {
        'order': '2300',
    },
    '273ac6682b8d42004baf9d8269da1520': {
        'order': '2500',
    },
    'a3d886282b8d42004baf9d8269da1580': {
        'order': '2600',
    },
    '9439c2282b8d42004baf9d8269da157d': {
        'order': '2900',
    },
    '974982682b8d42004baf9d8269da15d1': {
        'order': '3100',
    },
    'ccd597004fd85e00398ebc511310c751': {
        'order': '3200',
    },
    '40ea0a682b8d42004baf9d8269da15b9': {
        'order': '3400',
    },
    'f4539ae82b8d42004baf9d8269da159a': {
        'order': '3500',
    },
    '71931ee82b8d42004baf9d8269da15aa': {
        'order': '3800',
    },
    '7ba35ee82b8d42004baf9d8269da15b8': {
        'order': '4000',
    },
    'd7b653404fd85e00398ebc511310c77c': {
        'order': '4100',
    },
    '944108962b494200de14ff8119da15e4': {
        'order': '4300',
    },
    '848108962b494200de14ff8119da15e6': {
        'order': '4400',
    },
    'a7c100d62b494200de14ff8119da1587': {
        'order': '4700',
    },
    'e60484d62b494200de14ff8119da154e': {
        'order': '4900',
    },
    '0557df004fd85e00398ebc511310c795': {
        'order': '5000',
    },
    '0ea4ae35e05bd1004baf32cd1360103c': {
        'order': '5200',
    },
    'f66484d62b494200de14ff8119da1562': {
        'order': '5300',
    },
    '2fb448d62b494200de14ff8119da15f8': {
        'order': '5400',
    },
    '60150cd62b494200de14ff8119da1527': {
        'order': '5700',
    },
    '472588d62b494200de14ff8119da1500': {
        'order': '5900',
    },
    'b5a797404fd85e00398ebc511310c7a5': {
        'order': '6000',
    },
    'bab1c26a4ff20600398ebc511310c7ec': {
        'order': '6200',
    },
    'c2f14a6a4ff20600398ebc511310c7c3': {
        'order': '6300',
    },
    '41724a6a4ff20600398ebc511310c7d4': {
        'order': '6600',
    },
    '37824e6a4ff20600398ebc511310c77d': {
        'order': '6800',
    },
    'a618df404fd85e00398ebc511310c719': {
        'order': '6900',
    },
    '30352d034f101240398ebc511310c784': {
        'order': '7100',
    },
    'cb452d034f101240398ebc511310c7e6': {
        'order': '7200',
    },
    '38a52d034f101240398ebc511310c785': {
        'order': '7500',
    },
    'b6b769434f101240398ebc511310c7e2': {
        'order': '7700',
    },
    '71c5a58f4fdcde00398ebc511310c74d': {
        'order': '7800',
    },
    '1fdae29078a27100de14a0934907b542': {
        'order': '7900',
    },
    'fe46d37f0a0a8c070010a1d126c321b5': {
        'order': '8100',
    },
    'fa7942bf0a0a8c0701eccecc4ae639ae': {
        'order': '8200',
    },
    'fe4865dc0a0a8c0701a5af7d33a989d5': {
        'order': '8300',
    },
    '00baaa9078a27100de14a0934907b53e': {
        'order': '1900',
        'label': 'standard_volumes',
    },
    '399aa69078a27100de14a0934907b5f0': {
        'order': '2100',
        'label': 'standard_gigabytes',
    },
    'bbd82e5078a27100de14a0934907b54f': {
        'order': '800',
        'label': 'instances',
    },
    '6019ca282b8d42004baf9d8269da15aa': {
        'order': '2800',
        'label': 'cp1_volumes',
    },
    '6c5a46682b8d42004baf9d8269da15d2': {
        'order': '3000',
        'label': 'cp1_gigabytes',
    },
    'd2739ae82b8d42004baf9d8269da15fb': {
        'order': '3700',
        'label': 'cp2_volumes',
    },
    'b6f296e82b8d42004baf9d8269da156c': {
        'order': '3900',
        'label': 'cp2_gigabytes',
    },
    '81a1cc962b494200de14ff8119da1555': {
        'order': '4600',
        'label': 'cpio1_volumes',
    },
    'ab518c962b494200de14ff8119da1553': {
        'order': '4800',
        'label': 'cpio1_gigabytes',
    },
    '54f4c8d62b494200de14ff8119da15d9': {
        'order': '5600',
        'label': 'io1_volumes',
    },
    'e49448d62b494200de14ff8119da1554': {
        'order': '5800',
        'label': 'io1_gigabytes',
    },
    '33024a6a4ff20600398ebc511310c794': {
        'order': '6500',
        'label': 'wig-cp1_volumes',
    },
    '0c52ca6a4ff20600398ebc511310c754': {
        'order': '6700',
        'label': 'wig-cp1_gigabytes',
    },
    '6085ad034f101240398ebc511310c729': {
        'order': '7400',
        'label': 'wig-cpio1_volumes',
    },
    '3297a9434f101240398ebc511310c716': {
        'order': '7600',
        'label': 'wig-cpio1_gigabytes',
    },
    '26f8ea5078a27100de14a0934907b56e': {
        'order': '900',
        'label': 'cores',
    },
    '3a49629078a27100de14a0934907b552': {
        'order': '1100',
        'label': 'ram',
    },
    '27b6a65078a27100de14a0934907b5a4': {
        'order': '300',
        'label': 'projectname',
    },
    'b02b2e9078a27100de14a0934907b540': {
        'order': '8000',
        'label': 'comments',
    },
    'edf80e282b8d42004baf9d8269da154b': {
        'order': '2700',
        'label': 'cp1',
    },
    'c963dae82b8d42004baf9d8269da1561': {
        'order': '3600',
        'label': 'cp2',
    },
    'bc2088962b494200de14ff8119da15d6': {
        'order': '4500',
        'label': 'cpio1',
    },
    '73400c962b494200de14ff8119da1569': {
        'order': '5500',
        'label': 'io1',
    },
    '86ae73862b494200de14ff8119da1531': {
        'order': '1800',
        'label': 'standard',
    },
    '09d1022a4ff20600398ebc511310c7e9': {
        'order': '6400',
        'label': 'wig-cp1',
    },
    '01656d034f101240398ebc511310c732': {
        'order': '7300',
        'label': 'wig-cpio1',
    },
    'abc786282b8d42004baf9d8269da1572': {
        'order': '2400',
        'label': 'visible_cp1',
    },
    '8869c2282b8d42004baf9d8269da158b': {
        'order': '3300',
        'label': 'visible_cp2',
    },
    'e11148962b494200de14ff8119da153e': {
        'order': '4200',
        'label': 'visible_cpio1',
    },
    'e22280d62b494200de14ff8119da1536': {
        'order': '5100',
        'label': 'visible_io1',
    },
    '2871466a4ff20600398ebc511310c71c': {
        'order': '6100',
        'label': 'visible_wig-cp1',
    },
    '4e15a5034f101240398ebc511310c759': {
        'order': '7000',
        'label': 'visible_wig-cpio1',
    },
}

QUOTA_UPDATE_RP = {
      'volume_quota': [
          {
              'type': '86ae73862b494200de14ff8119da1531',
              'gigabytes': '399aa69078a27100de14a0934907b5f0',
              'volumes': '00baaa9078a27100de14a0934907b53e'
          },
          {
              'type': 'edf80e282b8d42004baf9d8269da154b',
              'gigabytes': '6c5a46682b8d42004baf9d8269da15d2',
              'volumes': '6019ca282b8d42004baf9d8269da15aa',
              'visible': 'abc786282b8d42004baf9d8269da1572'
          },
          {
              'type': 'c963dae82b8d42004baf9d8269da1561',
              'gigabytes': 'b6f296e82b8d42004baf9d8269da156c',
              'volumes': 'd2739ae82b8d42004baf9d8269da15fb',
              'visible': '8869c2282b8d42004baf9d8269da158b'
          },
          {
              'type': 'bc2088962b494200de14ff8119da15d6',
              'gigabytes': 'ab518c962b494200de14ff8119da1553',
              'volumes': '81a1cc962b494200de14ff8119da1555',
              'visible': 'e11148962b494200de14ff8119da153e'
          },
          {
              'type': '73400c962b494200de14ff8119da1569',
              'gigabytes': 'e49448d62b494200de14ff8119da1554',
              'volumes': '54f4c8d62b494200de14ff8119da15d9',
              'visible': 'e22280d62b494200de14ff8119da1536'
          },
          {
              'type': '09d1022a4ff20600398ebc511310c7e9',
              'gigabytes': '0c52ca6a4ff20600398ebc511310c754',
              'volumes': '33024a6a4ff20600398ebc511310c794',
              'visible': '2871466a4ff20600398ebc511310c71c'
          },
          {
              'type': '01656d034f101240398ebc511310c732',
              'gigabytes': '3297a9434f101240398ebc511310c716',
              'volumes': '6085ad034f101240398ebc511310c729',
              'visible': '4e15a5034f101240398ebc511310c759'
          },
      ],

      'instances': "bbd82e5078a27100de14a0934907b54f",
      'cores': "26f8ea5078a27100de14a0934907b56e",
      'ram': "3a49629078a27100de14a0934907b552",
      'project_name': "27b6a65078a27100de14a0934907b5a4",
      'comments': "b02b2e9078a27100de14a0934907b540",
    }
