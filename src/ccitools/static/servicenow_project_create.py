PROJECT_CREATION_RP = {
    'gigabytes': "c0bafe85e0d3d1004baf32cd136010e5",
    'volumes': "9edaf609e0d3d1004baf32cd136010d2",
    'instances': "8d123b70f1a389004baf946ac6346f41",
    'cores': "a4323b70f1a389004baf946ac6346fdb",
    'ram': "0e523b30f1a389004baf946ac6346f4e",
    'e_group': "59d2fb70f1a389004baf946ac6346f4a",
    'owner': "7233bf70f1a389004baf946ac6346f62",
    'department': "e5013770f1a389004baf946ac6346f21",
    'type': "1040d485843bd400151566f0c2cc3262",
    'project_name': "5f413770f1a389004baf946ac6346faf",
    'description': "7c917730f1a389004baf946ac6346f4a",
    'comments': "db65f319e057d1004baf32cd13601032",
    'other': "8b9a45522b894200de14ff8119da1526",
    'hep_flavors': "b70bb7b621605184adf9b5210b89fc6c",
    'accounting_group': "f1450d4f4f77524015d3bc511310c7ff",
    'project_type': "013be4bc4f73de0015d3bc511310c7ef",
    'other_ag': "a38df9114f0ce24015d3bc511310c770",
    }

PROJECT_CREATION_RP_MAPPING = {
   '75ffb13c115530404bafa32c34fb163e': {
        'order': '0',
    },
    '48010e3c115530404bafa32c34fb16fe': {
        'order': '100',
    },
    'b9622e634f895600e3a2119f0310c738': {
        'order': '200',
    },
    '590462a34f895600e3a2119f0310c76e': {
        'order': '300',
    },
    'fd24a6a34f895600e3a2119f0310c76a': {
        'order': '600',
    },
    'e4f276a74f895600e3a2119f0310c7e2': {
        'order': '900',
    },
    '703376274f895600e3a2119f0310c7b1': {
        'order': '1200',
    },
    '43c1f770f1a389004baf946ac6346f73': {
        'order': '1300',
    },
    'de493a09e0d3d1004baf32cd1360100f': {
        'order': '1400',
    },
    'eae17f30f1a389004baf946ac6346f75': {
        'order': '1500',
    },
    'bb92bb70f1a389004baf946ac6346fcb': {
        'order': '1800',
    },
    'b3593a09e0d3d1004baf32cd13601018': {
        'order': '2000',
    },
    'b84a7a09e0d3d1004baf32cd13601086': {
        'order': '2100',
    },
    '30faba09e0d3d1004baf32cd13601081': {
        'order': '2200',
    },
    '3d4bfa09e0d3d1004baf32cd1360100d': {
        'order': '2500',
    },
    '0ea4ae35e05bd1004baf32cd1360103c': {
        'order': '2600',
    },
    'e2ab3e09e0d3d1004baf32cd13601026': {
        'order': '2700',
    },
    'fe46d37f0a0a8c070010a1d126c321b5': {
        'order': '2900',
    },
    'fa7942bf0a0a8c0701eccecc4ae639ae': {
        'order': '3000',
    },
    'fe4865dc0a0a8c0701a5af7d33a989d5': {
        'order': '3100',
    },
    '8099acf84f73de0015d3bc511310c7dc': {
        'order': '3300',
    },
    'b15ae0bc4f73de0015d3bc511310c7be': {
        'order': '3400',
    },
    '593db1114f0ce24015d3bc511310c7a9': {
        'order': '3500',
    },
    '345d3d9d4fc8e24015d3bc511310c70e': {
        'order': '3800',
    },
    '1cb9a07c4f73de0015d3bc511310c7ea': {
        'order': '4000',
    },
    '8d123b70f1a389004baf946ac6346f41': {
        'order': '1600',
        'label': 'instances',
    },
    'a4323b70f1a389004baf946ac6346fdb': {
        'order': '1700',
        'label': 'cores',
    },
    '0e523b30f1a389004baf946ac6346f4e': {
        'order': '1900',
        'label': 'ram',
    },
    'c0bafe85e0d3d1004baf32cd136010e5': {
        'order': '2300',
        'label': 'gigabytes',
    },
    '9edaf609e0d3d1004baf32cd136010d2': {
        'order': '2400',
        'label': 'volumes',
    },
    'f1450d4f4f77524015d3bc511310c7ff': {
        'order': '3600',
    },
    'e5013770f1a389004baf946ac6346f21': {
        'order': '400',
        'label': 'department',
    },
    '013be4bc4f73de0015d3bc511310c7ef': {
        'order': '3900',
    },
    '1040d485843bd400151566f0c2cc3262': {
        'order': '3200',
        'label': 'priv',
    },
    'db65f319e057d1004baf32cd13601032': {
        'order': '2800',
        'label': 'comment',
    },
    '7c917730f1a389004baf946ac6346f4a': {
        'order': '800',
        'label': 'description',
    },
    '59d2fb70f1a389004baf946ac6346f4a': {
        'order': '1100',
        'label': 'egroup',
    },
    '5f413770f1a389004baf946ac6346faf': {
        'order': '700',
        'label': 'projectname',
    },
    'a38df9114f0ce24015d3bc511310c770': {
        'order': '3700',
    },
    '8b9a45522b894200de14ff8119da1526': {
        'order': '500',
        'label': 'other_department',
    },
    '7233bf70f1a389004baf946ac6346f62': {
        'order': '1000',
        'label': 'owner',
    },
}
