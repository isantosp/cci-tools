#!/usr/bin/python

import logging

from argparse import ArgumentParser
from ccitools.common import add_logging_arguments, configure_logging
from ccitools.errors import CciSnowTicketError
from ccitools.servicenow import ServiceNowClient
from ConfigParser import ConfigParser

def update_main(snowclient, ticket, comment, worknote):
    try:
        if comment:
           snowclient.add_comment(ticket, comment)
           logging.info("'%s' updated with the following comment: '%s'" % (ticket, comment))
        if worknote:
           snowclient.add_work_note(ticket, worknote)
           logging.info("'%s' updated with the following worknote: '%s'" % (ticket, worknote))
    except CciSnowTicketError, msg:
        logging.warning(str(msg))
        

def main():
    parser = ArgumentParser(description="Update a ServiceNow ticket")
    add_logging_arguments(parser)
    parser.add_argument("--ticket-number", required=True)
    parser.add_argument("--instance", default="cern", help="Service now instance")
    parser.add_argument("--comment", dest='comment')
    parser.add_argument("--worknote",dest='worknote')
    
    args = parser.parse_args()
    if not (args.comment or args.worknote):
        parser.error('Nothing to do. Add --comment or --worknote')

    configure_logging(args)

    config = ConfigParser()
    config.readfp(open('/etc/cci/ccitools.cfg'))

    snowclient = ServiceNowClient(config.get("servicenow", "user"), config.get("servicenow", "pass"),
                                  instance=args.instance)
    update_main(snowclient, args.ticket_number, args.comment, args.worknote)

if __name__ == "__main__":
    main()
