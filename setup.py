#!/usr/bin/env python

from distutils.core import setup

setup(name='cci-tools',
      version='0.3.6',
      description='Tools for the CERN Cloud Infrastructure team',
      author='Cloud Infrastructure Team',
      author_email='cloud-infrastructure-3rd-level@cern.ch',
      url='http://www.cern.ch/openstack',
      package_dir= {'': 'src'},
      packages=['ccitools', 'ccitools/static'],
      scripts=['scripts/cci-create-project',
               'scripts/cci-dump-project-request',
               'scripts/cci-health-report',
               'scripts/cci-notify-intervention',
               'scripts/cci-update-quota',
               'scripts/cci-update-ticket',
               'scripts/cci-sendmail'],
     )
