Name: cci-tools
Version: 0.3.6
Release: 2%{?dist}
Summary: Tools for the CERN Cloud Infrastructure team
Source0: %{name}-%{version}.tar.gz
BuildArch: noarch

Group: CERN/Utilities
License: Apache License, Version 2.0
URL: https://openstack.cern.ch/

BuildRequires: python-devel
Requires: ai-tools
Requires: python-cornerstoneclient
Requires: python-ldap
Requires: python-openstackclient
Requires: python-keystoneclient-kerberos
Requires: python-requests
Requires: python-suds
Requires: pytz
%if 0%{?el5} || 0%{?el6}
Requires: python-argparse
%endif

%description
Set of Python tools to be used by the Cloud Infrastructure Team

%prep
%setup -q


%build
CFLAGS="%{optflags}" %{__python} setup.py build

%install
%{__rm} -rf %{buildroot}
%{__python} setup.py install --skip-build --root %{buildroot}
install -m 755 scripts/cci-dump-project-request ${RPM_BUILD_ROOT}/usr/bin
install -m 755 scripts/cci-update-ticket ${RPM_BUILD_ROOT}/usr/bin
install -m 755 scripts/cci-update-quota ${RPM_BUILD_ROOT}/usr/bin
install -m 755 scripts/cci-notify-intervention ${RPM_BUILD_ROOT}/usr/bin

%clean
rm -rf ${RPM_BUILD_ROOT}

%files
%defattr (-, root, root)
%{_bindir}/cci-*
%{python_sitelib}/*
%doc

%changelog
* Thu Jan 12 2017 Daniel Fernandez Rodriguez <danielfr@cern.ch> - 0.3.6-1
- [cloud] Add glance client

* Tue Dec 6 2016 Daniel Fernandez Rodriguez <danielfr@cern.ch> - 0.3.5.1-1
- [xldap] get_primary_account supports primary, service, and secondary accounts

* Tue Dec 6 2016 Daniel Fernandez Rodriguez <danielfr@cern.ch> - 0.3.5-1
- [xldap] Add get_primary_account

* Thu Nov 30 2016 Daniel Fernandez Rodriguez <danielfr@cern.ch> - 0.3.3-1
- [cloudclient] Fix matching condition on get_hypervisor_by_name

* Thu Nov 28 2016 Daniel Fernandez Rodriguez <danielfr@cern.ch> - 0.3.2-1
- [cloudclient] Fix condition on get_hypervisor_by_name

* Thu Nov 15 2016 Daniel Fernandez Rodriguez <danielfr@cern.ch> - 0.3.1-1
- [cci-notify-intervention] Expose mailfrom mailcc & mailbcc to user

* Thu Oct 27 2016 Daniel Fernandez Rodriguez <danielfr@cern.ch> - 0.3-1
- [rudneckclient] Add missing json import
- [cloudclient] Fix cases where searching for an obj on openstack returns all matching elements
- [cci-notify-intervention] Also fix those cases

* Wed Oct 12 2016 Daniel Fernandez Rodriguez <danielfr@cern.ch> - 0.2-1
- [cloudclient] Changes to make ccitools work with latest aitools openstack auth version

* Thu Oct 6 2016 Daniel Fernandez Rodriguez <danielfr@cern.ch> - 0.1.1-1
- [snowclient] Add email to ticket watchlist

* Mon Aug 8 2016 Daniel Fernandez Rodriguez <danielfr@cern.ch> - 0.1-1
- [xldap] Add function to get_starting_with_egroup_members
- [snowclient] Add restriction to only update tickets assigned to groups the user belongs to
- [snowclient] Add new param 'resolver' to resolve_ticket so tickets get resolved/assigned by this user

* Tue Jul 19 2016 Daniel Fernandez Rodriguez <danielfr@cern.ch> - 0.0.50-2
- [cci-update-quota] Small update closing comment

* Tue Jul 19 2016 Daniel Fernandez Rodriguez <danielfr@cern.ch> - 0.0.50-1
- [cci-update-quota] Update resolving ticket message
- [xldap] Add couple of functions to get users and egroups names
- [snowclient] Add create_project_creation to servicenow client

* Mon Jun 22 2016 Daniel Fernandez Rodriguez <danielfr@cern.ch> - 0.0.49-1
- [snowclient] Add create INC
- [cci-create-project] Many changes to support project properties; accounting_group & type

* Mon May 2 2016 Daniel Fernandez Rodriguez <danielfr@cern.ch> - 0.0.48-1
- [cci-notify-intervention] Store VM responsible in lowercase to avoid case sensitive duplicates
- [cci-update-quota] Add comment before scalate quota update request

* Fri Apr 14 2016 Daniel Fernandez Rodriguez <danielfr@cern.ch> - 0.0.47-1
- [cloud lib] Verify quota update and bug fixes
- [cci-create-project] Remove HEP and enable project from summary table

* Wed Mar 30 2016 Daniel Fernandez Rodriguez <danielfr@cern.ch> - 0.0.46-1
- [cci_project_creation] Improve error messages
- [rundeck lib] SSL verify enabled by default
- [rundeck lib] takeover_schedule improvements
- [ldap lib] Fix cases where xldap query may result empty

* Mon Feb 29 2016 Daniel Fernandez Rodriguez <danielfr@cern.ch> - 0.0.45-1
- [xldap lib] Fix slowness when using is_existing_egroup function

* Wed Feb 17 2016 Daniel Fernandez Rodriguez <danielfr@cern.ch> - 0.0.44-1
- [cci-update-quota] Fix crash when volume_quota empty

* Fri Feb 5 2016 Daniel Fernandez Rodriguez <danielfr@cern.ch> - 0.0.43-1
- [servicenow lib] Add Create request and create_quota_update
- [servicenow lib] Code cleanups
- [cci-update-quota] Addapt script to new lib code

* Fri Jan 8 2016 Daniel Fernandez Rodriguez <danielfr@cern.ch> - 0.0.42-1
- [rudneck] Change takeover API endpoint (v14 and up)
- [cci-notify-intervention] Address cases where user does not exist
- [cci-update-quota] Get rid of HEP flavors config
- [cci-update-quota] Add users name to closing message
- [cci-update-quota] Get caller from u_caller_id instead of from sys_created_by
- [cci-update-quota] Fix IDs new 7th volume type

* Tue Nov 30 2015 Daniel Fernandez Rodriguez <danielfr@cern.ch> - 0.0.41-1
- [cci-create-project] Move instance argument where makes more sense
- [cci-notify-intervention] Fix empty user list

* Tue Nov 26 2015 Daniel Fernandez Rodriguez <danielfr@cern.ch> - 0.0.40-1
- [rundeck lib] Fix for takeover schedule
- [cci-tools scripts] Add argument 'instance' to all scripts
- [servicenow lib] Fix for those cases when Rundeck updates a ticket and there is no previous assignee
- [servicenow lib] Remove leading and trailing spaces when reading input from record producer

* Tue Nov 17 2015 Daniel Fernandez Rodriguez <danielfr@cern.ch> - 0.0.39-1
- [general lib] Logging handeling improvements
- [cci-update-quota] Fix "Apply project quota request" shows 0 instead of current value bug
- [rundeck lib] Add run execution by job id
- [rundeck lib] Allow custom headers in functions

* Fri Oct 16 2015 Daniel Fernandez Rodriguez <danielfr@cern.ch> - 0.0.38-1
- [servicenow lib] QuotaUpdateRequestRP more error resilient
- [fim lib] Add enable_project function

* Wed Oct 14 2015 Daniel Fernandez Rodriguez <danielfr@cern.ch> - 0.0.37-1
- [common lib] Check return code when ssh executing
- [cci-notify-intervention] Add vmslist variable
- [servicenow lib] Add new RP fields reflecting the new volume types (wig-cp1, wig-cpio1)

* Sun Oct 4 2015 Daniel Fernandez Rodriguez <danielfr@cern.ch> - 0.0.36-1
- [common lib] Add optional params to ssh_executor

* Mon Sep 28 2015 Daniel Fernandez Rodriguez <danielfr@cern.ch> - 0.0.35-1
- [servicenow lib] Make servicenow lib compatible with SNOW tables latest changes
- [servicenow lib] Small fixes

* Fri Sep 25 2015 Daniel Fernandez Rodriguez <danielfr@cern.ch> - 0.0.34-1
- [cci-notify-intervention] Fix problem with project IDs and names
- [rundeck lib] Fix typo
- [common lib] Add new function ssh_executor

* Wed Sep 23 2015 Daniel Fernandez Rodriguez <danielfr@cern.ch> - 0.0.33-1
- [cci-notify-users] ldap server address from an external variable
- [cci-update-quota] ldap server address from an external variable
- [common lib] cern_sso_get_cookie define cookie path as param

* Tue Sep 22 2015 Daniel Fernandez Rodriguez <danielfr@cern.ch> - 0.0.32-1
- [cci-notify-intervention] Better exceptions handling
- [cci-notify-intervention] Add mail-content-type option
- [cci-notify-intervention] ldap server address from an external variable

* Tue Sep 22 2015 Daniel Fernandez Rodriguez <danielfr@cern.ch> - 0.0.31-1
- [cci-notify-intervention] Set Nova as source of truth for VM's owner/responsible
- [cci-notify-users] Deprecated in favor of cci-notify-intervention
- [python-landbclient] Removed package dependency

* Wed Sep 16 2015 Daniel Fernandez Rodriguez <danielfr@cern.ch> - 0.0.30-1
- New improved version of cci-notify-intervention.
- Add python-landbclient dependency

* Wed Sep 9 2015 Daniel Fernandez Rodriguez <danielfr@cern.ch> - 0.0.29-1
- Fix cern_get_sso_cookie common function
- cloud library: Add search project by ID
- cci-notify-users: Projects can be referenced using names or IDs
- cci-create-project: Improve output messages

* Tue Sep 1 2015 Daniel Fernandez Rodriguez <danielfr@cern.ch> - 0.0.28-1
- Add cern_get_sso_cookie function to cci-tools common library
- Fix typo on cci-create-project

* Thu Aug 13 2015 Daniel Fernandez Rodriguez <danielfr@cern.ch> - 0.0.27-1
- Add support for volumes types quota updates

* Thu Aug 13 2015 Daniel Fernandez Rodriguez <danielfr@cern.ch> - 0.0.26-1
- Add FIMClient Library
- Update cci-create-project to use FIMClient
- Add default/force exec modes to cci-create-project
- Update ServiceNow Library to selfassign ticket before updating it

* Thu Aug 6 2015 Daniel Fernandez Rodriguez <danielfr@cern.ch> - 0.0.25-1
- Add takeover_schedule to Rundeck API Client
- Remove automatic health_check non compatible with SSO auth

* Tue Jul 14 2015 Daniel Fernandez Rodriguez <danielfr@cern.ch> - 0.0.24-1
- Support latest updates on "Request for shared Cloud Service Project" RP
- Improve servicenow library code
- Add get tickets by hostname to servicenow library
- Adapt scripts to latest library changes

* Mon Jun 29 2015 Daniel Fernandez Rodriguez <danielfr@cern.ch> - 0.0.23-1
- cci-update-quota: Monospace font for quota summary on snow tickets

* Fri Jun 5 2015 Daniel Fernandez Rodriguez <danielfr@cern.ch> - 0.0.22-1
- Improve Rundeck API library to support SSO endpoint auth

* Thu May 28 2015 Daniel Fernandez Rodriguez <danielfr@cern.ch> - 0.0.21-1
- cci-notify-intervention: Add OTG substitution, sort summary by vmname and show starting hour.
- Add dryrun/perform behaviour to cci-update-quota

* Thu May 07 2015 Daniel Fernandez Rodriguez <danielfr@cern.ch> - 0.0.20-1
- cci-notify-intervention: Add option to notify main user of the VM (not only owners).

* Wed May 06 2015 Daniel Fernandez Rodriguez <danielfr@cern.ch> - 0.0.19-1
- Add initial cci-notify-intervention script (NSC source, Hyperv oriented)
- xldap: add get_security_groups_members function.

* Thu Apr 30 2015 Daniel Fernandez Rodriguez <danielfr@cern.ch> - 0.0.18-1
- Fix cornerstone return code and message 
- Add egroup name validation

* Wed Apr 29 2015 Daniel Fernandez Rodriguez <danielfr@cern.ch> - 0.0.17-1
- Fix listing servers in hypervisor when no servers are found
- Add logging to sendmail client
- Fix issue when vm owner was not found

* Fri Apr 17 2015 Daniel Fernandez Rodriguez <danielfr@cern.ch> - 0.0.16-1
- Refactor the XldapClient & add unit tests.
- Add new variables (date, hour and duration) to cci-notify-users script.

* Thu Apr 02 2015 Daniel Fernandez Rodriguez <danielfr@cern.ch> - 0.0.15-1
- Fix raise exception and add print messages.

* Wed Apr 01 2015 Daniel Fernandez Rodriguez <danielfr@cern.ch> - 0.0.14-1
- Improvements to cci-update-quota script.
- New method to set state 'waiting for user' in ServiceNow
- New method in the cloud client to check flavour access.

* Fri Mar 25 2015 Daniel Fernandez Rodriguez <danielfr@cern.ch> - 0.0.13-1
- Add cci-update-quota script
- Add cci-notify-users script
- Fix SnowClient functionality interacting with tickets.
- Fix XldapClient get_egroup_email function.
- Add get_hypervisor_by_name to CloudClient

* Fri Mar 06 2015 Daniel Fernandez Rodriguez <danielfr@cern.ch> - 0.0.12-1
- New functions in CloudClient to get info about nova services

* Fri Mar 06 2015 Daniel Fernandez Rodriguez <danielfr@cern.ch> - 0.0.11-1
- Add generic writing method & additional snow funcitonality

* Fri Mar 06 2015 Luis Fernandez Alvarez <luis.fernandez.alvarez@cern.ch> - 0.0.10-1
- (cci-health-report) Add task & user_id column and make them default.
- (cci-health-report) Rename 'Last Update' column to 'Since'
- (cci-health-report) Refactor code

* Thu Feb 26 2015 Daniel Fernandez Rodriguez <danielfr@cern.ch> - 0.0.9-2
- Fix typo on snowclient

* Thu Feb 26 2015 Daniel Fernandez Rodriguez <danielfr@cern.ch> - 0.0.9-1
- Add resolve ticket and change FE to snowclient
- Cornerstone endpoint is read from config file 
- Add export job definitions and projects to rundeck API client

* Wed Feb 11 2015 Luis Fernandez Alvarez <luis.fernandez.alvarez@cern.ch> - 0.0.8-1
- (danielfr) Add rundeck api client
- (lfernand) Add subcommands to cci-create-project (from-input/from-snow)

* Fri Feb 06 2015 Daniel Fernandez Rodriguez <danielfr@cern.ch> 0.0.7-1
- Fix egroup checking for project creation

* Thu Feb 05 2015 Luis Fernandez Alvarez <luis.fernandez.alvarez@cern.ch> - 0.0.6-1
- Add cci-health-report to monitor vms & volume status

* Thu Feb 05 2015 Daniel Fernandez Rodriguez <danielfr@cern.ch> 0.0.5-1
- Fix cci-project-create to call Cornerstone

* Thu Jan 29 2015 Luis Fernandez Alvarez <luis.fernandez.alvarez@cern.ch> - 0.0.4-1
- (lfernand/danielfr) Add cci-create-project script
- (danielfr) Add get_egroup_email to xldap client

* Wed Jan 21 2015 Luis Fernandez Alvarez <luis.fernandez.alvarez@cern.ch> - 0.0.3-1
- Add CloudClient class to interoperate with the CERN cloud
- (danielfr) Add validations for servicenow integration
- (danielfr) Add emails utilities

* Wed Dec 17 2014 danielfr <danielfr@cern.ch> - 0.0.2-1
- Add xldap client
- Add method to write work_note in ServiceNow request

* Fri Dec 08 2014 Luis Fernandez Alvarez <luis.fernandez.alvarez@cern.ch> - 0.0.1-3
- Add RPM changelog

* Fri Dec 08 2014 Luis Fernandez Alvarez <luis.fernandez.alvarez@cern.ch> - 0.0.1-2
- Bump version (Koji fixes)

* Fri Dec 05 2014 Luis Fernandez Alvarez <luis.fernandez.alvarez@cern.ch> - 0.0.1-1
- [cci-dump-project-request] Initial version
- [cci-update-ticket] Initial version
- Add 'servicenow' module

* Tue Dec 02 2014 Luis Fernandez Alvarez <luis.fernandez.alvarez@cern.ch> - 0.0.0
- Initial version
