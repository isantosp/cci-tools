cci-tools
=========

CERN Cloud Infrastructure Tools (aka `cci-tools`) provides a set of Python 
scripts and libraries following good software & coding practices, ensuring as
much as possible the reusability of the code.

Release Management
------------------

### Changelog

In general, all the merge requests done in the repository should cover a new
feature, bug fix, etc... triggering a new version of the package.

This implies that every new feature should:

* Add a new entry in `cci-tools.spec`.
* Bump the version in `cci-tools.spec` and `setup.py`.

### Version

New version of the application should be tagged in the repository using the
following command:

    git tag -a 0.0.1-3 79fa2d4 -m "cci-tools-0.0.1-3"

This will make commands like `git describe` returns the right version when the
tag is checked out.

### Koji builds

The Koji builds can be easily triggered pointing to the git repository with a
given label. As mentioned in the previous point, release tags should contain
code versions ready to build.

    koji build cci7-utils git+https://gitlab.cern.ch/cloud-infrastructure/cci-tools.git#0.0.1-3

The previous build will be only available in `testing` until we tag it for the
`qa` repository.

    koji tag-pkg cci7-utils-qa cci-tools-0.0.1-3.el7

If you're happy with the qa version, you can release it in `stable`

    koji tag-pkg cci7-utils-stable cci-tools-0.0.1-3.el7

Finally, an entry in `Makefile.koji` has been added to facilitate the build
task. Assuming you are place in the tag you want to build:

    git checkout 0.0.1-3
    make -f Makefile.koji build
